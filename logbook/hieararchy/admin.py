from django.contrib import admin
from .models import *

admin.site.register(MachineType)

admin.site.register(Site)

admin.site.register(Company)

admin.site.register(ProcessUnit)

# Register your models here.
