from __future__ import unicode_literals

from django.db import models as m

MAX_NAME_LENGTH = 64

# MARK: Macros

def NameField():
    return m.CharField(max_length=MAX_NAME_LENGTH)

def DescriptionField():
    return m.CharField(max_length=255)

def ImagePathField():
    return m.CharField(max_length=128)

class Company(m.Model):
    companyName = NameField()
    companyDescription = DescriptionField()
    companyWebsite = m.CharField(max_length=255)
    companyLogoName = ImagePathField()
    def __unicode__(self):
        return self.companyName


# MARK: Static tables

class SystemType(m.Model):
    systemTypeAbbr = m.CharField(max_length=6)
    systemTypeName = NameField()
    systemTypeIconName = ImagePathField()
    def __unicode__(self):
        return self.systemTypeName

class MachineType(m.Model):
    machineTypeAbbr = m.CharField(max_length=6)
    machineTypeName = NameField()
    machineTypeIconName = ImagePathField()
    def __unicode__(self):
        return self.machineTypeName

class ComponentType(m.Model):
    componentTypeAbbr = m.CharField(max_length=6)
    componentTypeName = NameField()
    componentTypeIconName = ImagePathField()
    def __unicode__(self):
        return self.componentTypeName

class TagType(m.Model):
    tagTypeName = m.CharField(max_length = 64)


class DeviceModels(m.Model):
    deviceModelName = NameField()
    deviceModelVersion = m.CharField(max_length=6)
    deviceModelDataEncoding = m.CharField(max_length = 64)

class Device(m.Model):
    deviceModel = m.ForeignKey(DeviceModels, on_delete = m.PROTECT)
    deviceUUID = m.CharField(max_length=64)
    deviceOwner = m.IntegerField() #TODO: Relation to device owner

    tagTypeIconName = ImagePathField()

# MARK: Machine hierarchy

class Site(m.Model):
    siteCompany = m.ForeignKey(Company, on_delete = m.CASCADE)
    siteName = NameField()
    siteLocation = m.CharField(max_length=256)
    siteContact = m.CharField(max_length=256)
    def __unicode__(self):
        return self.siteName + ', ' + self.siteCompany.companyName

class ProcessUnit(m.Model):
    processUnitSite = m.ForeignKey(Site, on_delete = m.CASCADE)
    processUnitName = NameField()
    processUnitDescription = m.CharField(max_length=255)
    processUnitLocation = m.CharField(max_length=255)

class System(m.Model):
    systemProcessUnit = m.ForeignKey(ProcessUnit, on_delete = m.CASCADE)
    systemName = NameField()
    systemType = m.ForeignKey(SystemType, null=True, on_delete = m.SET_NULL)

class FunctionalLocation(m.Model):
    functionalLocationSystem = m.ForeignKey(System, on_delete = m.CASCADE)
    functionalLocationName = m.CharField(max_length = 64)
    functionalLocationDescription = m.CharField(max_length=255)

class Machine(m.Model):
    equipmentName = NameField()
    machineType = m.ForeignKey(MachineType, null=True, on_delete = m.SET_NULL)

    '''Date: when the machine was last monitored'''
    def lastMeasurementDate():
        pass #TODO: machine last monitored

    '''Analysis of the last measurement'''
    def lastMeasurementAnalysis():
        pass #TODO: machine last data

class Component(m.Model):
    componentMachine = m.ForeignKey(Machine, on_delete = m.CASCADE)
    componentType = m.ForeignKey(ComponentType, null=True, on_delete = m.SET_NULL)
    componentFrequency = m.IntegerField()

class Tag(m.Model):
    tagMachine = m.ForeignKey(Machine, null=True, on_delete=m.SET_NULL)
    tagDateIssued = m.DateTimeField()
    tagDateDeprecated = m.DateTimeField()
    tagType = m.ForeignKey(TagType, on_delete = m.PROTECT)
    tagData= m.CharField(max_length=12)

    '''Bool: Whether the tag was being used at the given date'''
    def isValid(date):
        pass    #TODO: IMPLEMENT COMPARISON
        return true

# MARK: Data

class Measurement(m.Model):
    measurementTag = m.ForeignKey(Tag, on_delete = m.PROTECT)
    measurementData = m.BinaryField()
    measurementTime = m.DateTimeField()
    measurementImportedTime = m.DateTimeField()
    measurementUser = m.ForeignKey("auth.user")
    measurementDevice = m.ForeignKey(Device)


# MARK: User structure



class UserRole(m.Model):
    userRoleUser = m.ForeignKey("auth.user")
    userRoleSite = m.ForeignKey(Site, on_delete = m.CASCADE)
    userRolePrivileges = m.IntegerField() # TODO: user privileges table
